(async ()=>{
    console.log('gateway staring');
    try {
        await require('./index')();
        console.log('gateway was started');
    }
    catch (e) {
        console.log('gateway was not started', e);
    }
})();