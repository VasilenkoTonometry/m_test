async function getEvents(req, reply) {
    try {
        const response = await req.hemera.act({
            topic: 'events',
            cmd: 'get',
            day: req.query.day,
            month: req.query.month,
            timeout$: 60000
        });
        reply.send({
            success: 1,
            data: response.data
        });
    }
    catch (e) {
        console.log(e);
        reply.send({
            success: 0
        });
    }
}

module.exports = {getEvents};