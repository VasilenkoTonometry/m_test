const fastify = require('fastify')({
    logger: true
});
const cors = require('fastify-cors');
const fistifyHemera = require('fastify-hemera');
const config = require('../config/production');
const {getEvents} = require('./handlers/wiki_handlers');

module.exports = async () => {
    await fastify.register(cors);
    await fastify.register(fistifyHemera, {
        hemera: {
            name: 'gateway',
            logLevel: 'error',
            childLogger: true,
            tag: 'hemera-gateway'
        },
        nats: {
            url: process.env.NATS_URL || config.nats.url,
        }
    });

    fastify.get('/api/v1/get-events/', {
        handler: getEvents
    });

    fastify.listen(config.gateway.port, config.gateway.address, (err, address) => {
        if (err) throw err;
        fastify.log.info(`server listening on ${address}`);
    });

};