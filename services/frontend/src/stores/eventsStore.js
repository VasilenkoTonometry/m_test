import {action, observable} from "mobx";
import axios from "axios";

class EventsStore {
    @observable day = null;
    @observable month = null;
    @observable loading = false;
    @observable day_event = null;

    @action setDate(day, month) {
        this.day = day;
        this.month = month;
        this.loading = true;
        axios.get(`${process.env.REACT_APP_API_URL}/api/v1/get-events/`, {
            params: {
                day: day,
                month: month
            }
        }).then(res => {
            this.loading = false;
            this.day_event = res.data.data;
        }).catch(() => {
            this.day_event = null;
            this.loading = false;
        })
    }

}

export default EventsStore;