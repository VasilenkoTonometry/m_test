import React, {useState} from "react";
import {CircleButton} from "../../components/Buttons";
import Load from "../../components/Loader";

export default (props) => {

    const [open, setOpen] = useState(window.innerWidth >= 768);

    function resize() {
        setOpen(window.innerWidth >= 768);
    }

    window.addEventListener("resize", resize);

    return <div className={`main-layout ${open ? 'menu-opened' : 'menu-closed'}`}>
        <div className="main-layout__left-menu">
            <div className="left-menu__day">
                {props.day}
            </div>
            <div className="left-menu__month">
                {props.month}
            </div>
        </div>
        <div className="main-layout__content">
            {props.content}
        </div>
        <div className="main-layout__right-menu">
            <header className="right-menu__header">
                <div className="right-menu__header-title">
                    События
                </div>
                <div className="right-menu__actions">
                    <CircleButton onClick={() => {
                        setOpen(!open)
                    }}><i className="fas fa-bars"/></CircleButton>
                </div>
            </header>
            <div className="right-menu__event">
                {props.loading && <Load />}
                {props.dayEvent && !props.loading ? props.dayEvent.title : "Нет данных"}
            </div>
            <div className={`main-layout__menu-trigger ${open ? 'hidden' : 'shown'}`}>
                <CircleButton onClick={() => {
                    setOpen(!open)
                }}><i className="fas fa-bars"/></CircleButton>
            </div>
        </div>
    </div>
}