import React from "react";

export default (props) => {
    return <div className="ratio">
        <div className={`ratio-inner ratio-${props.ratio}`}>
            <div className="ratio-content">
                {props.children}
            </div>
        </div>
    </div>
}