import React from "react";

export const CircleButton = (props) => <button className="button circle_button" {...props}>
    {props.children}
</button>;