import React, {useState} from "react";
import moment from "moment/min/moment-with-locales.min";
import {CircleButton} from "./Buttons";
import Ratio from "../components/Ratio";

const DayNames = () => <div className="calendar__day-names">
    <div className="container">
        <div className="row">
            <div className="col">
                <div className="calendar__day">
                    Пн
                </div>
            </div>
            <div className="col">
                <div className="calendar__day">
                    Вт
                </div>
            </div>
            <div className="col">
                <div className="calendar__day">
                    Ср
                </div>
            </div>
            <div className="col">
                <div className="calendar__day">
                    Чт
                </div>
            </div>
            <div className="col">
                <div className="calendar__day">
                    Пт
                </div>
            </div>
            <div className="col">
                <div className="calendar__day">
                    Сб
                </div>
            </div>
            <div className="col">
                <div className="calendar__day">
                    Вс
                </div>
            </div>
        </div>
    </div>
</div>;

const Week = (props) => {
    let days = [];

    let {
        date,
    } = props;

    const {
        month,
        selected,
        select,
    } = props;

    for (let i = 0; i < 7; i++) {
        let day = {
            name: date.format("dd").substring(0, 1),
            number: date.date(),
            isCurrentMonth: date.month() === month.month(),
            isToday: date.isSame(new Date(), "day"),
            date: date
        };
        days.push(
            <Day day={day}
                 selected={selected}
                 select={select}
                 key={i}
            />
        );

        date = date.clone();
        date.add(1, "day");
    }

    return <div className="calendar__week" key={days[0]}>
        <div className="container">
            <div className="row">
                {days}
            </div>
        </div>
    </div>

};

const Day = (props) => {
    const {
        day,
        day: {
            date,
            isCurrentMonth,
            isToday,
            number
        },
        select,
        selected
    } = props;
    return <div className="col">
        <Ratio ratio="1-1">
            <div
                key={date.toString()}
                className={"day" + (isToday ? " today" : "") + (isCurrentMonth ? "" : " different-month") + (date.isSame(selected) ? " selected" : "")}
                onClick={() => select(day)}>{number}
            </div>
        </Ratio>
    </div>
};

export default (props) => {
    const localMonth = moment();
    localMonth.locale('ru');

    const initSelectedDate = () => {
        const {onDateSelected} = props;
        if (typeof onDateSelected === "function") {
            const params = {
                day: parseInt(month.format("DD")),
                month: month.format("MMMM")
            };
            onDateSelected(params);
        }
        return moment().startOf('day')
    };

    const [month, setMonth] = useState(localMonth);
    const [selected, setSelected] = useState(initSelectedDate);

    const previous = () => {
        const new_month = month.clone().subtract(1, 'month');
        setMonth(new_month);
    };

    const next = () => {
        const new_month = month.clone().add(1, 'month');
        setMonth(new_month);
    };

    const select = (day) => {
        const new_date = day.date.clone();
        setSelected(new_date);
        setMonth(new_date);
        const {onDateSelected} = props;
        if (typeof onDateSelected === "function") {
            const params = {
                day: parseInt(new_date.format("DD")),
                month: new_date.format("MMMM")
            };
            onDateSelected(params);
        }
    };

    const capitalize = (s) => {
        if (typeof s !== 'string') return ''
        return s.charAt(0).toUpperCase() + s.slice(1)
    };

    const renderWeeks = () => {
        let weeks = [];
        let done = false;
        let date = month.clone().startOf("month").add("w" - 1).day("Sunday");
        let count = 0;
        let monthIndex = date.month();

        while (!done) {
            weeks.push(
                <Week key={date}
                      date={date.clone()}
                      month={month}
                      select={(day) => select(day)}
                      selected={selected}/>
            );

            date.add(1, "w");

            done = count++ > 2 && monthIndex !== date.month();
            monthIndex = date.month();
        }

        return weeks;
    };

    const renderMonthLabel = () => {
        return <span className="calendar__month-label">{capitalize(month.format("MMMM YYYY"))}</span>;
    };


    return <section className="calendar">
        <header className="calendar__header">
            <div className="calendar__month-display">
                <CircleButton onClick={previous}>
                    <i className="arrow fa fa-angle-left"/>
                </CircleButton>
                {renderMonthLabel()}
                <CircleButton onClick={next}>
                    <i className="arrow fa fa-angle-right"/>
                </CircleButton>
            </div>
            <DayNames/>
        </header>
        {renderWeeks()}
    </section>
};