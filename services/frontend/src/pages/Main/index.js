import React from "react";
import MainLayout from "../../layout/Main"
import Calendar from "../../components/Calendar";
import {observer, inject} from "mobx-react";

export default inject("eventsStore")(observer((props) => {
    const {eventsStore} = props;
    return <MainLayout
        day={eventsStore.day}
        month={eventsStore.month}
        dayEvent={eventsStore.day_event}
        loading={eventsStore.loading}
        content={
            <div className="main-page">
                <Calendar onDateSelected={({day, month}) => {
                    eventsStore.setDate(day, month);
                }}/>
            </div>
        }
    />
}));