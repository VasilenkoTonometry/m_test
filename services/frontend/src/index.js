import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "mobx-react";
import App from './App';
import EventsStore from "./stores/eventsStore";

const stores = {
    eventsStore: new EventsStore()
};


ReactDOM.render(<Provider {...stores}><App/></Provider>, document.getElementById('root'));

