import React from 'react';
import './styles/App.sass';
import MainPage from "./pages/Main";

function App() {
    return (
        <div className="App">
            <MainPage/>
        </div>
    );
}

export default App;
