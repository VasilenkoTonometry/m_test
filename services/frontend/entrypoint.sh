#!/usr/bin/env bash
echo "REACT_APP_API_URL=$REACT_APP_API_URL" >> ./.env
echo "npm install started"
npm install
echo "npm install complete"
npm run build
echo "build finished"
npm install -g serve
serve -s -p 80 build
echo "server was started"