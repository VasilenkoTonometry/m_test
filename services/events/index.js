const config = require('../config/production');
const Hemera = require('nats-hemera');
const HemeraJoi = require('hemera-joi');
const nats = require('nats').connect(process.env.NATS_URL || config.nats.url);
const {getHolidays} = require('./handlers/wiki_handlers');

const hemera = new Hemera(nats, {
    logLevel: 'error',
    childLogger: true,
    tag: 'hemera-events'
});

module.exports = async ()=>{
    hemera.use(HemeraJoi);
    await hemera.ready();
    const Joi = hemera.joi;

    hemera.add({
        topic: 'events',
        cmd: 'get',
        day: Joi.number().required(),
        month: Joi.string().required()
    }, getHolidays)

};