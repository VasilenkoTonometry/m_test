const axios = require('axios');
const redis = require("async-redis");
const config = require("../../config/production");
const client = redis.createClient(process.env.REDIS_URL || config.redis.url);

client.on("error", function (err) {
    console.log("Error " + err);
});

async function getHolidays(req) {
    //https://www.mediawiki.org/wiki/API:Categorymembers

    const genitive = {
        "январь": "января",
        "февраль": "февраля",
        "март": "марта",
        "апрель": "апреля",
        "май": "мая",
        "июнь": "июня",
        "июль": "июля",
        "август": "августа",
        "сентябрь": "сентября",
        "октябрь": "октября",
        "ноябрь": "ноября",
        "декабрь": "декабря"
    };

    function getCategory(params) {
        return new Promise(async resolve => {
            try {
                let finish = false;

                let result = [];

                while (!finish) {
                    const url = "https://ru.wikipedia.org/w/api.php?origin=*";
                    const response = await axios.get(url, {params}).then(res => res.data).catch(() => resolve(null));
                    const push = response && response.query && response.query.categorymembers && response.query.categorymembers.length > 0;
                    if (push) response.query.categorymembers.map(item => result.push(item));
                    if (response === null || !(response.hasOwnProperty("continue"))) finish = true;
                    else params.cmcontinue = response.continue.cmcontinue;

                }

                return resolve(result);
            } catch (e) {
                console.log(e);
                return resolve(null);
            }
        });
    }

    function getExternalLinks(params) {
        return new Promise(async resolve => {
            try {

                let finish = false;
                let result = {};

                while (!finish) {
                    const url = "https://ru.wikipedia.org/w/api.php?origin=*";
                    const response = await axios.get(url, {params}).then(res => res.data).catch(() => resolve(null));
                    const push = response && response.query && response.query.pages;
                    if (push) {
                        const pages = Object.keys(response.query.pages);
                        pages.map(key => {
                            if (
                                result.hasOwnProperty(key)
                            ) {
                                const count = response.query.pages[key].hasOwnProperty("extlinks") ? response.query.pages[key].extlinks.length : 0;
                                result[key].count += count;
                            } else {
                                const count = response.query.pages[key].hasOwnProperty("extlinks") ? response.query.pages[key].extlinks.length : 0;
                                result[key] = {
                                    count: count,
                                    title: response.query.pages[key].title
                                };
                            }
                        });
                    }
                    if (response === null || !(response.hasOwnProperty("continue"))) {
                        finish = true;
                    } else params.elcontinue = response.continue.elcontinue;
                }
                return resolve(result);
            } catch (e) {
                console.log(e);
                return resolve(null);
            }
        });
    }

    const redisKey = `${req.month.toString()}_${req.day.toString()}`;
    const cachedResponse = await client.get(redisKey);

    if (cachedResponse) {
        console.log('get from cache');
        return JSON.parse(cachedResponse);
    } else {

        const ct_params = {
            action: "query",
            list: "categorymembers",
            cmtitle: `Категория:Праздники_${req.day}_${genitive[req.month.toLowerCase()]}`,
            format: "json",
            cmtype: "page"
        };

        const categorymembers = await getCategory(ct_params);
        const get_events = !(categorymembers && categorymembers.length > 0);

        if (get_events) {

            const ct_params = {
                action: "query",
                list: "categorymembers",
                cmtitle: `Категория:События_${req.day}_${genitive[req.month.toLowerCase()]}`,
                format: "json",
                cmtype: "page",
                cmlimit: 500
            };

            //вариант без дополнительного запроса на страничку
            const response = await getCategory(ct_params);
            //end

            const titles = response.map(item => item.title).filter(item => item !== undefined).join("|");

            const lnk_params = {
                action: "query",
                prop: "extlinks",
                titles: titles,
                format: "json",
            };

            const links = await getExternalLinks(lnk_params);

            let max = null;

            Object.keys(links).map(key => {
                if (max === null || links[key].count > max.count) max = links[key];
            });

            await client.set(redisKey, JSON.stringify(max));

            return max;
        } else {

            const titles = categorymembers.map(item => item.title).join("|");
            const lnk_params = {
                action: "query",
                prop: "extlinks",
                titles: titles,
                format: "json",
            };

            const links = await getExternalLinks(lnk_params);

            let max = null;

            Object.keys(links).map(key => {
                if (max === null || links[key].count > max.count) max = links[key];
            });

            await client.set(redisKey, JSON.stringify(max));

            return max;
        }

    }
}

module.exports = {getHolidays};