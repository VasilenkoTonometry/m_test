(async ()=>{
    console.log('events service starting');
    try {
        await require('./index')();
        console.log('events service was started')
    }
    catch (e) {
        console.log('events service was not started', e);
    }
})();