const gateway = require('./gateway/index');
const events = require('./events/index');

(async ()=>{
    gateway();
    events();
})();